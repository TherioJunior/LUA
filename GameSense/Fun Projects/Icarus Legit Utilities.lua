local icarus = { };
icarus.refs = { };
icarus.items = { };
icarus.globals = { };

icarus.refs.bhop = ui.reference("MISC", "Movement", "Bunny hop");

ui.new_label("LUA", "A", "--- [ Icarus Legit Utilities ] ---");
icarus.items.failurerate = ui.new_slider("LUA", "A", "Failure rate", 1, 99, 0, 1, "%");
icarus.items.switch_knife_hand = ui.new_checkbox("LUA", "A", "Switch Knife Hand");
ui.new_label("LUA", "A", "--- [ Icarus Legit Utilities ] ---");

icarus.globals.changed = false;

local function on_setup_cmd(cmd)
	-- credits: https://gamesense.pub/forums/viewtopic.php?id=42231
	if(ui.get(icarus.refs.bhop) and client.key_state(0x20)) then
		local failrate = ui.get(icarus.items.failurerate);

		if(client.random_int(1, 100) <= failrate) then
			cmd.in_jump = 0;

			client.delay_call(0.09, function(cmd)
				client.in_jump = 1;
			end)
		end	
	else
		cmd.in_jump = 0;
	end
end

local function on_run_cmd(cmd)
	if(not ui.get(icarus.items.switch_knife_hand)) then
		return;
	end

	icarus.globals.me = entity.get_local_player();
	icarus.globals.my_weapon = entity.get_player_weapon(icarus.globals.me);
	icarus.globals.my_weapon_class = entity.get_classname(icarus.globals.my_weapon);

    if(icarus.globals.my_weapon_class == "CKnife") then
        cvar.cl_righthand:set_raw_int(cvar.cl_righthand:get_string() == "0" and 1 or 0)
        icarus.globals.changed = true
    elseif icarus.globals.changed then
        cvar.cl_righthand:set_raw_int(tonumber(cvar.cl_righthand:get_string()))
        icarus.globals.changed = false
    end
end

ui.set_callback(icarus.refs.bhop, function(self)
	local update_cb = ui.get(self) and client.set_event_callback or client.unset_event_callback;
	update_cb("setup_command", on_setup_cmd);
end)

ui.set_callback(icarus.items.switch_knife_hand, function(self)
	local update_cb = ui.get(self) and client.set_event_callback or client.unset_event_callback;
	update_cb("run_command", on_run_cmd);
end)

client.set_event_callback("shutdown", function()
    if(icarus.globals.changed) then
        cvar.cl_righthand:set_raw_int(tonumber(cvar.cl_righthand:get_string()))
    end
end)
