local screenX, screenY = client.screen_size()

client.set_event_callback("paint", function()
    if(not _G.IcarusConfigurationSystem.IsFeatureLUA1Enabled) then
        return;
    end

    local nonWeapons = {
        "CKnife",
        "CSmokeGrenade",
        "CFlashbang",
        "CHEGrenade",
        "CDecoyGrenade",
        "CIncendiaryGrenade"
    }
    local me = entity.get_local_player()
    local myWeapon = entity.get_player_weapon(me)
    local myWeaponClass = entity.get_classname(myWeapon)

    for _, class in pairs(nonWeapons) do
        if(myWeaponClass == class) then
            return;
        end
    end

    if(me == nil or entity.is_alive(me) == false) then
        return;
    end

    local posX, posY = screenX / 2 + 1, screenY / 2 + 20
	local eyeX, eyeY, eyeZ = client.eye_position()
	local pitch, yaw = client.camera_angles()

    local entExists, wallDmg, multiplier = false, 0, 8192

    local sinPitch = math.sin(math.rad(pitch))
    local cosPitch = math.cos(math.rad(pitch))
    local sinYaw = math.sin(math.rad(yaw))
    local cosYaw = math.cos(math.rad(yaw))

    local directionVector = {
        cosPitch * cosYaw,
        cosPitch * sinYaw,
        -sinPitch
    }

    local fraction, _ = client.trace_line(
        me,
        eyeX,
        eyeY,
        eyeZ,
        eyeX + (directionVector[1] + multiplier),
        eyeY + (directionVector[2] + multiplier),
        eyeZ + (directionVector[3] + multiplier)
    )

    if(fraction < 1) then
        local entIndex1, dmg = client.trace_bullet(
            me,
            eyeX,
            eyeY,
            eyeZ,
            eyeX + (directionVector[1] * (multiplier * fraction + 128)),
            eyeY + (directionVector[2] * (multiplier * fraction + 128)),
            eyeZ + (directionVector[3] * (multiplier * fraction + 128))
        )

        if(entIndex1 ~= nil) then
            entExists = true
        end

        if(wallDmg < dmg) then
            wallDmg = dmg
        end
    end

    if(wallDmg > 0) then
        renderer.text(posX, posY, 124, 195, 13, 255, "cb", 0, tostring(wallDmg))
    end
end)
