local icarus = { };
icarus.globals = { };

client.set_event_callback("run_command", function()
    if(not _G.IcarusConfigurationSystem.IsFeatureLUA2Enabled) then
        return;
    end

    icarus.globals.me = entity.get_local_player();
    icarus.globals.my_weapon = entity.get_player_weapon(icarus.globals.me);
    icarus.globals.my_weapon_class = entity.get_classname(icarus.globals.my_weapon);

    if(icarus.globals.my_weapon_class == "CKnife") then
        cvar.cl_righthand:set_raw_int(cvar.cl_righthand:get_string() == "0" and 1 or 0)
        icarus.globals.changed = true
    elseif icarus.globals.changed then
        cvar.cl_righthand:set_raw_int(tonumber(cvar.cl_righthand:get_string()))
        icarus.globals.changed = false
    end
end)
